function map(myArray, callback) {
  if (!myArray || !callback) return false;
  resultArray = [];
  for (let index = 0; index < myArray.length; index++) {
    resultArray.push(callback(myArray[index], index, myArray));
  }
  return resultArray;
}
// console.log(
//   map([1, 2, 3, 4], function(val) {
//     return val * 2;
//   })
// );

module.exports = map;
