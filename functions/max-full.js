function maxFullDifference(arr) {
  if (
    !arr ||
    arr.every(function(i) {
      return typeof i === 'string';
    })
  )
    return false;

  let smallest = arr[0];
  let biggest = arr[0];

  for (let i = 1; i < arr.length; i++) {
    if (arr[i] < smallest) smallest = arr[i];
    if (arr[i] > biggest) biggest = arr[i];
  }
  diff = biggest - smallest;
  return !isNaN(diff) ? diff : false;
}

console.log(maxFullDifference());

module.exports = maxFullDifference;
