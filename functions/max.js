function maxDifference(arr) {
  if (
    !arr ||
    arr.length == 0 ||
    arr.every(function(i) {
      return typeof i === 'string';
    })
  )
    return false;
  let biggestDiff = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] > arr[i + 1]) {
      biggestDiff =
        arr[i] - arr[i + 1] > biggestDiff ? arr[i] - arr[i + 1] : biggestDiff;
    } else {
      biggestDiff =
        arr[i + 1] - arr[i] > biggestDiff ? arr[i + 1] - arr[i] : biggestDiff;
    }
  }
  return isNaN(biggestDiff) ? false : biggestDiff;
}

console.log(maxDifference(['hoho', 'koko', 'toto']));
module.exports = maxDifference;
