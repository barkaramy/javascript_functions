function sort(array) {
  if (!array) return false;
  let pos = 0;
  let max = array[0];
  let min = array[0];
  arraySorted = [];

  for (let i = 0; i < array.length; i++) {
    if (max < array[i]) {
      max = array[i];
    }
  }

  for (let j = 0; j < array.length; j++) {
    for (let k = 0; k < array.length; k++) {
      if (array[k] !== null) {
        if (min > array[k]) {
          min = array[k];
          pos = k;
        }
      }
    }
    arraySorted[j] = min;
    array[pos] = null;
    min = max;
  }

  return arraySorted;
}

console.log(sort([]));

module.exports = sort;
