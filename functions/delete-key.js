function deleteKey(obj, key) {
  if (!obj && !key) {
    return false;
  } else if (!key) {
    return obj;
  } else if (obj === undefined) {
    return false;
  } else {
    let keyObj = key;
    let arrayOfkeys = Object.keys(obj);
    let a = null;
    arrayOfkeys.forEach((element) => {
      if (element == keyObj) {
        delete obj[element];
        a = obj;
      }
    });

    return a;
  }
}

console.log(deleteKey({}, ''));

module.exports = deleteKey;
