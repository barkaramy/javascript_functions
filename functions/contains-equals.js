function containsEquals(array) {
  if (!array || array.length === 0) return false;
  for (let i = 0; i < array.length; i++) {
    for (let j = i + 1; j < array.length; j++) {
      if (array[i] === array[j]) return array[i];
    }
  }
}

console.log(containsEquals([1, 3, 'test', 10, 'test']));

module.exports = containsEquals;
