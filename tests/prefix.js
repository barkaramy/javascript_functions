const { assert } = require('chai');
const { describe, it } = require('mocha');

const stringCheck = require('./../functions/prefix')

module.exports = describe('#1 - Créer une fonction qui préfixe un mot avec Car', function() {
  it('should prefix with Car', function() {
    assert.equal(stringCheck('autobus'), 'Carautobus')
    assert.equal(stringCheck('carrément'), 'Carcarrément')
    assert.equal(stringCheck('carrelage'), 'Carcarrelage')
    assert.equal(stringCheck('antilope'), 'Carantilope')
    assert.equal(stringCheck('je mange'), 'Carje mange')    
  });

  it('should not prefix with Car', function() {
    assert.equal(stringCheck('Carrément'), 'Carrément')
    assert.equal(stringCheck('Carrelage'), 'Carrelage')
    assert.equal(stringCheck('Car'), 'Car')
  });

  it('should not prefix and return', function() {
    assert.equal(stringCheck(null), false)
    assert.equal(stringCheck(undefined), false)
    assert.equal(stringCheck(''), false)
  });
});