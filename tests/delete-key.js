const { assert, expect } = require('chai');
const { describe, it } = require('mocha');

const deleteKey = require('./../functions/delete-key')

module.exports = describe('#3 - Créer une fonction qui supprime la clé donnée d\'un objet et le retourne', function() {
  it('should delete the given key', function() {
    expect(deleteKey({ name: 'Jack', job: 'Surfeur'}, 'job')).to.deep.equal({ name: 'Jack' })
    expect(deleteKey({ city: 'Bordeaux', address: 'Cours Victor Hugo'}, 'city')).to.deep.equal({ address: 'Cours Victor Hugo' })
  });

  it('should do nothing', function() {
    expect(deleteKey({ name: 'Jack', job: 'Surfeur'}, null)).to.deep.equal({ name: 'Jack', job: 'Surfeur'})
    expect(deleteKey({ name: 'Jack', job: 'Surfeur'}, undefined)).to.deep.equal({ name: 'Jack', job: 'Surfeur'})
    expect(deleteKey({ name: 'Jack', job: 'Surfeur'}, '')).to.deep.equal({ name: 'Jack', job: 'Surfeur'})
    expect(deleteKey({}, null)).to.deep.equal({})
    assert.equal(deleteKey(null, null), false)
    assert.equal(deleteKey(undefined, 'job'), false)
  });
});