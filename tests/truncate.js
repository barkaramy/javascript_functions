const { assert } = require('chai');
const { describe, it } = require('mocha');

require('./../functions/truncate')()

module.exports = describe('#1 - Créer une fonction qui coupe un mot à une position donnée', function() {
  it('should truncate hello world!', function() {
    assert.equal('Hello world!'.truncate(2), 'He...');
    assert.equal('Hello world!'.truncate(5), 'Hello...');
    assert.equal('Hello, world!'.truncate(5), 'Hello...');
  });

  it('should not truncate Hello', function() {
    assert.equal('Hello'.truncate(5), 'Hello');
  });

  it('should truncate Hello!', function() {
    assert.equal('Hello!'.truncate(5), 'Hello...');
  });

  it('should not truncate Hi!', function() {
    assert.equal('Hi!'.truncate(5), 'Hi!');
  });

  it('should not truncate Empty', function() {
    assert.equal(''.truncate(5), false);
  });

  it('should not include punctutation nor space', function() {
    assert.equal('Hello world!'.truncate(6), 'Hello...');
    assert.equal('Hello, world!'.truncate(6), 'Hello...');
  });
});